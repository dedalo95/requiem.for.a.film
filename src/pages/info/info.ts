import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the InfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
  name: 'infoPage'
})
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  crewRequiem
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.crewRequiem=[
      {
        nome:"Torquemada",
        img:"assets/imgs/torquemada.png",
        desc:"22 anni, studende di scienze agrarie",
        link:"http://requiemforafilm.com/2018/03/21/torquemada/"
      },
      {
        nome:"Kowalski",
        img:"assets/imgs/kowalski.jpg",
        desc:"21 anni, studente di economia e gestione aziendale.",
        link:"http://requiemforafilm.com/2018/03/21/kowalski/"  
      },
      {
        nome:"Don Malakas",
        img:"assets/imgs/don_malakas.jpg",
        desc:"22 anni, studente di economia e gestione aziendale, trader.",      
        link:"http://requiemforafilm.com/2018/03/21/don_malakas/"  
      },
      {
        nome:"Solzimer",
        img:"assets/imgs/solzimer.jpg",
        desc:"Thomas Tramarin, 22 anni, laureato in storytelling, scrittore. ",
        link:"http://requiemforafilm.com/2018/03/21/solzimer/"  
      },
      {
        nome:"Shangai",
        img:"assets/imgs/shangai.jpg",
        desc:"22 anni, studente di scienze motorie e sportiva",
        link:"http://requiemforafilm.com/2018/03/21/shangai/"  
      }
    ];
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad InfoPage');
  }

}
