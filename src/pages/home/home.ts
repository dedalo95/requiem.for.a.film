import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {HttpModule} from "@angular/http";
import {Http} from "@angular/http";
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { HttpParams } from '@angular/common/http';
import he from "he";
import { getHtmlTagDefinition } from '@angular/compiler';
import { ToastController } from 'ionic-angular';
import {Slides} from "ionic-angular";
import {ViewChild} from '@angular/core';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player';
import { MoviesPage } from '../movies/movies';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  arrayPosts;
  filter;
  form;
  array;
  loading;
  ctrl0;
  ctrl1;
  ctrl2;
  flipper;
  apiKey;
  playlistKey;
  videos;

  @ViewChild(Slides) slides: Slides;
  constructor(public http:HttpClient,public navCtrl: NavController,private toastCtrl: ToastController,private youtube: YoutubeVideoPlayer) {
    this.array=[];
    this.arrayPosts=[];
    this.filter={};
    this.form=true;
    this.loading=true;
    this.ctrl0=false;
    this.ctrl1=true;
    this.ctrl2=false;
    this.flipper=true;
    this.apiKey="AIzaSyCxQ3CFieKpMjBjCR6TKsoGy02riywnwGY";
    this.playlistKey="PLAsfSAUVffDpjvL4TZ6_JyEh_aAxSCFUn";
    this.videos=[];
  }
  showContent(){
    this.navCtrl.push("infoPage");
  }
  chooseImg(){
    var index=Math.floor((Math.random() * 5));
    console.log(index);
    switch(index){
      case 0:{
        return "https://srv.latostadora.com/designall.dll/cooking_time--i:13562336712901356232017092622;b:f8f8f8;s:H_A22;f:f;k:1960defbd8e3279e42d221edb86427bc;p:1.jpg";
      }
      case 1:{
        return "https://srv.latostadora.com/designall.dll/lesorcista--i:1356237825410135623191;k:47e1a490070134e6e1b52f0df1a79101;b:f8f8f8;s:M_M1;f:f.jpg";
      }
      case 2:{
        return "https://srv.latostadora.com/designall.dll/io_sono_il_pericolo--i:1356235383570135623201709261;k:058ed7a87fb894e24cce1672055b85d6;b:f8f8f8;s:H_A1;f:f.jpg";
      }
      case 3:{
        return "https://srv.latostadora.com/designall.dll/clockwork_orange_cantando_sotto_la_pioggia--i:13562316829301356232017092613;b:f8f8f8;s:H_A13;f:f;k:32c794a5ed779ee9669e78f1d6369a33;p:1.jpg";
      }
      case 4:{
        return "https://srv.latostadora.com/designall.dll/sfumature_of_the_future_anakin--i:1356238006170135623343131;k:177811b6575ecbe90990029492957a11;b:f8f8f8;s:H_X1;f:f.jpg";
      }
      case 5:{
        return "https://srv.latostadora.com/designall.dll/aliens--i:1356237788730135623201709261;b:f8f8f8;s:H_A1;f:f;k:2c848ab61ea2785651bee7f928f3bbe7;p:1.jpg";
      }
    }
  }
  spliceArr(index){
    var url=this.chooseImg();
    this.arrayPosts.splice(index,0,{
      titolo:"Acquistalo su Toradora!",
      url:"http://www.tostadora.it/t-shirt-cinema-serie-tv.php?a_aid=2015i113",
      img:url,
      text:"Scopri di più"
    });
  }
  slideChanged() {
    let currentIndex = this.slides.getActiveIndex();
    switch(currentIndex){
      case 0:{
        this.ctrl0=true;
        this.ctrl1=false;
        this.ctrl2=false;
        break;
      }
      case 1:{
        this.ctrl0=false;
        this.ctrl1=true;
        this.ctrl2=false;
        break;
      }
      case 2:{
        this.ctrl0=false;
        this.ctrl1=false;
        this.ctrl2=true;
        break;
      }
      default:{
        this.ctrl0=false;
        this.ctrl1=true;
        this.ctrl2=false;
        break;
      }
    }
  }
  access(){
    this.loading=false;
    var jsonSearch;
    //console.log(this.filter);
    this.form=false;
    var nomestar;
    var cognomestar;
    var nomeCompletoStar;
    if(this.filter.nomeAttore!=null && this.filter.cognomeAttore!=null &&this.filter.nomeAttore!=""){
      nomestar=this.filter.nomeAttore.toLowerCase();
      nomestar=nomestar.replace(/\s/g, '');
      cognomestar=this.filter.cognomeAttore.toLowerCase();
      cognomestar=cognomestar.replace(/\s/g, '');
      nomeCompletoStar=" #requiem"+nomestar+cognomestar;
    }
    jsonSearch={
      genere:this.filter.genere,
      star:nomeCompletoStar,
      crew:this.filter.crew
    }
    this.filter.nomeAttore="";
    this.filter.cognomeAttore="";
    this.filter.crew=""
    console.log(jsonSearch)
    this.getTag(jsonSearch);
  }
  takeGender(genere){
    let headers = new HttpHeaders();
    headers.append('Content-Type', "text");
    headers.append('projectid', "ChatApp");
    this.http.get("https://www.instagram.com/explore/tags/"+genere+"/?__a=1", { headers: headers, responseType: 'text' }).subscribe((response)=>{
      var edges=JSON.parse(response).graphql.hashtag.edge_hashtag_to_media.edges;
      if(edges.length>0){
        for(var i in edges){
          this.array.push({
            media_id:edges[i].node.id,
            text:edges[i].node.edge_media_to_caption.edges[0].node.text,
            link:edges[i].node.display_url
          });
        }        
      }
      var param=this.array;
      this.array=[];
      this.loading=true;
      if(param.length>0)
        this.navCtrl.push("moviesPage",param);
      else{
        this.loading=true;
        let toast = this.toastCtrl.create({
          message: 'Non ci sono film che soddisfano la tua ricerca...',
          duration: 3000,
          position: 'top'});
        toast.present();
      }
    });
  }
  takeAll(genere,star,crew){
    let headers = new HttpHeaders();
    headers.append('Content-Type', "text");
    headers.append('projectid', "ChatApp");
    this.http.get("https://www.instagram.com/explore/tags/"+star.split("#")[1]+"/?__a=1", { headers: headers, responseType: 'text' }).subscribe((response)=>{
      var edges=JSON.parse(response).graphql.hashtag.edge_hashtag_to_media.edges;
      console.log(edges)
      if(edges.length>0){
        for(var i in edges){
          var checkArray=edges[i].node.edge_media_to_caption.edges[0].node.text.match(/(^|\s)(#[a-z\d-]+)/ig)
          //se ci sono hashtag
          if(checkArray!=null){
            console.log(checkArray);
            //se la crew è presente
            if(crew!=null && crew!=undefined){
              //controlla tutto
              if(checkArray.includes(crew)&&checkArray.includes(" #"+genere)){
                this.array.push({
                  media_id:edges[i].node.id,
                  text:edges[i].node.edge_media_to_caption.edges[0].node.text,
                  link:edges[i].node.display_url
                });
              }
            }
            else{
              //controlla solo genere
              if(checkArray.includes(" #"+genere)){
                console.log("ci sono")
                this.array.push({
                  media_id:edges[i].node.id,
                  text:edges[i].node.edge_media_to_caption.edges[0].node.text,
                  link:edges[i].node.display_url
                });
              }
            }
          }
        }
      }
      var param=this.array;
      this.loading=true;
      this.array=[];
      if(param.length>0)
        this.navCtrl.push("moviesPage",param);
      else{
        this.array=[];
        this.loading=true;
        let toast = this.toastCtrl.create({
          message: 'Non ci sono film che soddisfano la tua ricerca...',
          duration: 3000,
          position: 'top'});
        toast.present();
      }
    },(err)=>{
      this.loading=true;
        let toast = this.toastCtrl.create({
          message: 'Qualcosa è andato storto... riprovaci!',
          duration: 3000,
          position: 'top'});
        toast.present();
    });
  }
  takeCrew(genere,crew){
    let headers = new HttpHeaders();
    headers.append('Content-Type', "text");
    headers.append('projectid', "ChatApp");
    this.http.get("https://www.instagram.com/explore/tags/"+crew.split("#")[1]+"/?__a=1", { headers: headers, responseType: 'text' }).subscribe((response)=>{
      var edges=JSON.parse(response).graphql.hashtag.edge_hashtag_to_media.edges;
      if(edges.length>0){
        for(var i in edges){
          var checkArray=edges[i].node.edge_media_to_caption.edges[0].node.text.match(/(^|\s)(#[a-z\d-]+)/ig);
          if(checkArray.includes(" #"+genere)){
            this.array.push({
              media_id:edges[i].node.id,
              text:edges[i].node.edge_media_to_caption.edges[0].node.text,
              link:edges[i].node.display_url
            });
          }
        }
      }
      var param=this.array;
      this.array=[];
      this.loading=true;
      if(param.length>0)
        this.navCtrl.push("moviesPage",param);
      else{
        this.loading=true;
        let toast = this.toastCtrl.create({
          message: 'Non ci sono film che soddisfano la tua ricerca...',
          duration: 3000,
          position: 'top'});
        toast.present();
      }
    });
  }
  getTag(json){
    //se il genere è definito
    if(json.genere!=undefined&&json.genere!=null&&json.genere!=""){
      //se c'è solo il genere
      if(json.star==undefined&&json.star!=null&&json.star!=""&&json.crew==undefined&&json.crew!=null&&json.crew!=""){
        //prendi solo genere
        this.takeGender(json.genere);
      }
      //altrimenti
      else{
        //se la star è definita
        if(json.star!=undefined&&json.star!=null&&json.star!=""){
          //se crew è definito
          if(json.crew!=undefined&&json.crew!=null&&json.crew!=""){
            //prendi tutto
            this.takeAll(json.genere,json.star,json.crew)
          }
          //altrimenti
          else{
            //prendi genere e star
            this.takeAll(json.genere,json.star,null)
          }
        }
        //altrimenti
        else{
          //se la crew è definita
          if(json.crew!=undefined&&json.crew!=null&&json.crew!=""){
            //prendi genere e crew
            this.takeCrew(json.genere,json.crew);
          }
          else{
            //prendi solo genere
            this.takeGender(json.genere);
          }
        }
      }
    }
    else{
      //se il genere non è definito mostra toast
      const toast = this.toastCtrl.create({
        message: 'Inserisci un genere!',
        duration: 2000
      });
      toast.present();
      this.loading=true;
    }
  }
  scrape(html){
    var dataExp = /window\._sharedData\s?=\s?({.+);<\/script>/;
    try {
      var dataString = html.match(dataExp)[1];
      var json = JSON.parse(dataString);
  }
  catch(e) {
      return null
  }

  return json;
  }
  openVideo(video){
    console.log(video.link);
    this.youtube.openVideo(video.link)
  }
  retrievePost(){
        let headers = new HttpHeaders();
        headers.append('Content-Type', "text");
        headers.append('projectid', "ChatApp");
        this.http.get("http://requiemforafilm.com/wp-json/wp/v2/posts?_embed", { headers: headers, responseType: 'text' }).subscribe((response)=>{
          var array=JSON.parse(response);
          for(var i in array){
            if(array[i].status=="publish"){
              //console.log(array[i]);
              var titolo=he.decode(array[i].title.rendered);
              //console.log(titolo)
              this.arrayPosts.push({
                titolo:titolo,
                url:array[i].link,
                img:array[i]._embedded["wp:featuredmedia"][0].source_url,
                text:"Vai all'articolo"
              });
            }
          }
          var index=Math.floor((Math.random() * 9));
          console.log(index); 
          this.spliceArr(index);
          //console.log(this.arrayPosts);
        },function(error) { return "error";});
  }

  ionViewDidLoad(){
    this.retrievePost();
    this.http.get("https://www.googleapis.com/youtube/v3/playlistItems?playlistId="+this.playlistKey+"&maxResults=25&part=snippet%2CcontentDetails&key="+this.apiKey)
    .subscribe((response)=>{
        var strVids=JSON.stringify(response);
        var vids=JSON.parse(strVids);
        console.log(vids);
        for(var i in vids.items){
          if(vids.items[i].contentDetails.videoId!=undefined){
            this.videos.push({
              img:vids.items[i].snippet.thumbnails.high.url,
              title:vids.items[i].snippet.title,
              description:vids.items[i].snippet.description,
              link:vids.items[i].contentDetails.videoId
            });
          }
        }
    });
  }
}
